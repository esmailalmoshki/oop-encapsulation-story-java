---
focus: src/main/java/com/epam/codingstory/Person.java
---
### Encapsulation

So now you have witnessed the application of the encapsulation principle in action:
- We have hidden all the [fields](src/main/java/com/epam/codingstory/Person.java:4-5) and implementation details from the client code.
- We have established our own [terms](src/main/java/com/epam/codingstory/Person.java:32-42) of what input values our class accepts.
- We have established a protocol of how client code may [get](src/main/java/com/epam/codingstory/Person.java:15-21) and [alter](src/main/java/com/epam/codingstory/Person.java:23-30) state of instances of our class.

All of that changes developed [Person](src/main/java/com/epam/codingstory/Person.java:3) from a plain structure to a truly **object-oriented** class.

### The end

package com.epam.codingstory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonTest {

    @Test
    void personTest(){
        Person person = new Person("Umberto", 33);

        assertEquals("Umberto", person.getName());
        assertEquals(33, person.getAge());

        person.incrementAge();

        assertEquals("Umberto", person.getName());
        assertEquals(34, person.getAge());
    }
}
# OOP Encapsulation Story
**To read**: [https://codingstories.io/story/https:%2F%2Fgitlab.com%2Fefimchik-codingstories%2Foop-encapsulation-story-java]

**Estimated reading time**: 60 min

## Story Outline
This story demonstrates basic application of OOP encapsulation principle.
It illustrates motivation for encapsulation and usage of related basic Java tools.  

## Story Organization
**Story Branch**: master
> `git checkout master`

Tags: #oop